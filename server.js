const express = require('express');
const app = express();
const path = require('path');
const cookieParser = require('cookie-parser');
const session = require('express-session');


app.use(cookieParser());
app.use(session({
    secret: 'ams23ndd-jjeydv56',
    resave: false,
    saveUninitialized: false,
    cookie: {
        secure: false,
        httpOnly: true,
        maxAge: 3600000
    }



}));
app.use('/public', express.static(path.join( __dirname, 'www') ));

app.get('/', (req, res) => {
    if (req.session.user == undefined){
        res.redirect('/login');
        res.send('No session exists for this user...');

    }else {      
        res.redirect('/dashboard');
    }
 
});
app.get('/create', (req, res) => {
    req.session.user = JSON.stringify({
        name:'Tina'

    })
    res.send('session created')

 });

 app.get('/dashboard', (req, res) => {
    res.send('dashboard was created')

 });
 app.get('/login', (req, res) => {
    req.session.counter = 0;
    res.send('Session was created');

});

app.listen(4000, () => console.log('app running in port 4000'));
